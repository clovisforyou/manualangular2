import { Component } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AppService]
})
export class AppComponent {
  titulo: string = 'Ejemplo Data binding';
  list: any[] = [
  	{
  		"id":"1",
  		"restaurante":"KFC",
      "estilo":"alert alert-danger",
     
  	},
  	{
  		"id":"2",
  		"restaurante":"Mcdonalds",
      "estilo":"alert alert-success",
      
  	},
  	{
  		"id":"3",
  		"restaurante":"Ginos",
      "estilo":"alert alert-danger",
     
  	}
  ];
  constructor(private appService: AppService){}

  useService(nombre: string)
  {
    this.appService.imprimirPorConsola(nombre);
  }
}
