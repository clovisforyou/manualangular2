import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  titulo: string = 'Directive For';
  list: any[] = [
  	{
  		"id":"1",
  		"restaurante":"KFC"
  	},
  	{
  		"id":"2",
  		"restaurante":"Mcdonalds"
  	},
  	{
  		"id":"3",
  		"restaurante":"Ginos"
  	}
  ];
}
