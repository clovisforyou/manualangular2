import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  titulo: string = 'Ejemplo Data binding';
  list: any[] = [
  	{
  		"id":"1",
  		"restaurante":"KFC",
      "estilo":"alert alert-danger"
  	},
  	{
  		"id":"2",
  		"restaurante":"Mcdonalds",
      "estilo":"alert alert-success"
  	},
  	{
  		"id":"3",
  		"restaurante":"Ginos",
      "estilo":"alert alert-danger"
  	}
  ];
}
