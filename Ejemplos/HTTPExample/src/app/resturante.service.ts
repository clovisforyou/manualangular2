import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class RestauranteService
{
	constructor(private http: Http){}

	guardarRestaurantes(resturantes: any[])
	{
		return this.http.put('https://manualangular.firebaseio.com/data.json',resturantes);

	}

	obtenerRestaurantes()
	{
		return this.http.get('https://manualangular.firebaseio.com/data.json').map(
			(response: Response) =>
			{
				const data = response.json();
				
				return data;
			}
			);
	}
}