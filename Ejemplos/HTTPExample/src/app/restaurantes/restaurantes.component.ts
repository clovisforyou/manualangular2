import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppService } from '../app.service';
import { FormGroup,FormControl } from '@angular/forms';
import { RestauranteService } from '../resturante.service';
import 'rxjs/Rx';


@Component({
  selector: 'app-restaurantes',
  templateUrl: './restaurantes.component.html',
  styleUrls: ['./restaurantes.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [AppService]
})
export class RestaurantesComponent implements OnInit {

   
   list: any[] = [
    {
      "id":"1",
      "restaurante":"KFC",
      "estilo":"alert alert-danger",
    },
    {
      "id":"2",
      "restaurante":"Mcdonalds",
      "estilo":"alert alert-success",
      
    },
    {
      "id":"3",
      "restaurante":"Ginos",
      "estilo":"alert alert-danger",
    }
  ];

  agregarRestauranteForm: FormGroup;
  constructor(private appService: AppService, private restauranteService: RestauranteService){}

  useService(nombre: string)
  {
    this.appService.imprimirPorConsola(nombre);
  }

  ngOnInit()
  {
      this.agregarRestauranteForm = new FormGroup({
        'id': new FormControl(null),
        'nombre': new FormControl(null),
        'color': new FormControl(null)
      }
        );
  }

  onSubmit()
  {
    this.list.push(
    {
      'id': this.agregarRestauranteForm.value.id,
      'restaurante': this.agregarRestauranteForm.value.nombre,
      'estilo': this.agregarRestauranteForm.value.color
    }
      );
    
  }

  onSave()
  {
    this.restauranteService.guardarRestaurantes(this.list).subscribe(
        (response) => console.log(response),
        (error) => console.log(error)
      );
  }

  onGet()
  {
    this.restauranteService.obtenerRestaurantes().subscribe(
        (restaurantes: any) => {
          console.log(restaurantes);
          this.list = restaurantes;
          
        },
        (error) => console.log(error)
      );
  }
}
