import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { CartaComponent } from './carta/carta.component';
import { RestaurantesComponent } from './restaurantes/restaurantes.component';
import { RestauranteService } from './resturante.service';
const appRoutes: Routes = [
  { path: '', component: RestaurantesComponent },
  { path: 'restaurante-carta/:id', component: CartaComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CartaComponent,
    RestaurantesComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    HttpModule
  ],
  providers: [RestauranteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
