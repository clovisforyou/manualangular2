import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppService } from '../app.service';

@Component({
  selector: 'app-restaurantes',
  templateUrl: './restaurantes.component.html',
  styleUrls: ['./restaurantes.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [AppService]
})
export class RestaurantesComponent implements OnInit {

   
   list: any[] = [
    {
      "id":"1",
      "restaurante":"KFC",
      "estilo":"alert alert-danger",
    },
    {
      "id":"2",
      "restaurante":"Mcdonalds",
      "estilo":"alert alert-success",
      
    },
    {
      "id":"3",
      "restaurante":"Ginos",
      "estilo":"alert alert-danger",
    }
  ];
  constructor(private appService: AppService){}

  useService(nombre: string)
  {
    this.appService.imprimirPorConsola(nombre);
  }

  ngOnInit()
  {

  }

}
