import { Pipe, PipeTransform } from '@angular/core';



@Pipe({ name: 'redRestaurants',
pure: false })
export class redRestaurantsPipe implements PipeTransform {
  transform(value: any) {
    return value.filter(restaurant => restaurant.estilo === 'alert alert-danger');
  }
}