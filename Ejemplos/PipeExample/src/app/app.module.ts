import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { redRestaurantsPipe } from './redpipe.pipe';

import { AppComponent } from './app.component';
import { CartaComponent } from './carta/carta.component';
import { RestaurantesComponent } from './restaurantes/restaurantes.component';

const appRoutes: Routes = [
  { path: '', component: RestaurantesComponent },
  { path: 'restaurante-carta/:id', component: CartaComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CartaComponent,
    RestaurantesComponent,
    redRestaurantsPipe
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
