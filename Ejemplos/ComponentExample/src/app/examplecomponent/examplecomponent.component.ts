
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-examplecomponent',
  templateUrl: './examplecomponent.component.html',
  styleUrls: ['./examplecomponent.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ExamplecomponentComponent implements OnInit {
  component: string = 'Primer Componente';
  constructor() { }

  ngOnInit() {
  }

}
