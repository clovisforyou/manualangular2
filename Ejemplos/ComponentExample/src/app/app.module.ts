import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ExamplecomponentComponent } from './examplecomponent/examplecomponent.component';

@NgModule({
  declarations: [
    AppComponent,
    ExamplecomponentComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
