import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-carta',
  templateUrl: './carta.component.html',
  styleUrls: ['./carta.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CartaComponent implements OnInit {
  listCarta: any[] = [
    {
      "id":"1",
      "restaurante":"KFC",
      "carta": [
      {"nombrePlato":"Bucket Mediano", "precio": "15"},
      {"nombrePlato":"Bucket Grande", "precio": "25"},
      ],
    },
    {
      "id":"2",
      "restaurante":"Mcdonalds",
      "carta": [
      {"nombrePlato":"Big Mac", "precio": "6"},
      {"nombrePlato":"Cbo", "precio": "8"},
      ],
      
    },
    {
      "id":"3",
      "restaurante":"Ginos",
      "carta": [
      {"nombrePlato":"Pasta carbonara", "precio": "10"},
      {"nombrePlato":"Pizza artesanal", "precio": "9"},
      ],
    }
  ];
  restauranteActual: any;

  id: number;
  idobservable: number;

  constructor(private route:ActivatedRoute) { }

  ngOnInit() {
  	this.id = this.route.snapshot.params['id'];
  	this.restauranteActual = this.listCarta[this.id-1];
  		
}

}
