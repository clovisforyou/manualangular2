import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppService } from '../app.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { Observer } from 'rxjs/Observer';

@Component({
  selector: 'app-restaurantes',
  templateUrl: './restaurantes.component.html',
  styleUrls: ['./restaurantes.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [AppService]
})
export class RestaurantesComponent implements OnInit {

   
   list: any[] = [
    {
      "id":"1",
      "restaurante":"KFC",
      "estilo":"alert alert-danger",
    },
    {
      "id":"2",
      "restaurante":"Mcdonalds",
      "estilo":"alert alert-success",
      
    },
    {
      "id":"3",
      "restaurante":"Ginos",
      "estilo":"alert alert-danger",
    }
  ];
  constructor(private appService: AppService){}

  useService(nombre: string)
  {
    this.appService.imprimirPorConsola(nombre);
  }

  ngOnInit()
  {
    /*
    const numeros = Observable.interval(1000);

    numeros.subscribe(
       (number: number) =>
       {
         console.log(number);
       }
      );
      */
      const desdeCeroObservable = Observable.create(
        (observer: Observer<string>) => 
        {
           setTimeout( () =>
           {
               observer.next('Primer comentario');
           },2000);

           setTimeout( () =>
           {
               observer.complete();
           },4000);

           setTimeout( () =>
           {
               observer.error('Algo fue mal');
           },6000);

           
             
           
        }
        );

      desdeCeroObservable.subscribe(
          (data: string) =>
          {
            console.log(data);
          },
          (error: string) =>
          {
            console.log(error);
          },
          () => {
            console.log('Termino');
          }
        );
  }

}
